(define (fact x)
  (if (eq? 0 x) 1 
                (* x (fact (- x 1)))))

(define (make-adder x)
  (lambda (y) (+ y x)))

(define speed 100)

(fact 10)

((make-adder 10) 10)

speed