﻿using System.Diagnostics;
using Scheme;

var environment = Scheme.Environment.Default();

var parser = new Parser();

var program = parser.ParseFile("testapp.scm");

var results = program.Select(sexp => sexp.Evaluate(environment, null));

foreach(var sexp in results) 
    Console.WriteLine(sexp);