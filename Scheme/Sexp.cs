﻿using System.Text;
namespace Scheme;

/// <summary>
/// The symbolic expression, it can be one of:
/// number
/// bool
/// symbol
/// string
/// callable
/// </summary>
public abstract class Sexp
{
    public abstract override string ToString();
    public abstract Sexp Evaluate(Environment env, SexpList args);
    public virtual bool ToBool() => throw new SexpException("not a bool");
    public virtual float ToNum() => throw new SexpException("not a number");
    public virtual string ToSymbol() => throw new SexpException("not a symbol");
    public virtual SexpProc ToProc() => throw new SexpException("not a proc");
    public virtual SexpList ToList() => throw new SexpException("not a list");
}

public class SexpNum : Sexp
{
    public virtual float Value { get; set; }

    public override string ToString() => Value.ToString();

    public override Sexp Evaluate(Environment env, SexpList args)
    {
        return this;
    }

    public override float ToNum() => Value;
}

public class SexpBool : Sexp
{
    public virtual bool Value { get; init; }

    public override string ToString() => Value ? "#t": "#f";

    public override Sexp Evaluate(Environment env, SexpList args)
    {
        return this;
    }

    public override bool ToBool() => Value;
}

public class SexpSymbol : Sexp
{
    public virtual string Value { get; init; } = "";

    public override string ToString() => Value;

    public override Sexp Evaluate(Environment env, SexpList args)
    {
        var resolution = env.Lookup(Value);

        if (resolution is null)
            throw new SexpException($"{Value} not defined");
        
        return resolution;
    }

    public override string ToSymbol() => Value;
}

public class SexpList : Sexp
{
    public Sexp[] List { get; set; }
    public Sexp Car
    {
        get => List[0];
        set => List[0] = value;
    }

    public SexpList Cdr => new() { List = List[1..] };
    
    public override string ToString()
    {
        var builder = new StringBuilder();
        builder.Append("(");
        foreach (var sexp in List)
        {
            builder.AppendFormat("{0} ", sexp);
        }
        builder.Append(")");
        return builder.ToString();
    }

    public override Sexp Evaluate(Environment env, SexpList args)
    {
        Sexp maybeCallable = SexpUndefined.Undefined;
        switch (Car)
        {
            case SexpSymbol symbol:
                switch (symbol.Value)
                {
                    case "if":
                        // Evaluating if:
                    // First the predicate shall be evaluated, it is the first element of Cdr
                        var ifArgs = Cdr;
                        var predicateResult = ifArgs.Car.Evaluate(env, null);
                        return predicateResult switch
                        {
                            SexpBool b when b.Value == false =>
                                ifArgs.List.Count() == 3? ifArgs.List[2].Evaluate(env, null) : SexpUndefined.Undefined,
                            _ => ifArgs.List[1].Evaluate(env, null),
                        };
                    // If we're evluating define, then we dont want to evaluate the first
                    // argument, as its a special form
                    case "define":
                        return env.Define(Cdr);
                    // If we're evaluating lambda then it receives formal parameters, which is a list of symbols
                    // those are not to be evaluated
                    case "lambda":
                        var cdr = Cdr;
                        var argsList = cdr.Car as SexpList;
                        if (argsList is null)
                            throw new SexpException("lambda expects list of symbol");
                        var argsSymbolList = argsList.List.Select(arg =>
                            arg is SexpSymbol sym ? sym : throw new SexpException("lambda expects list of symbol"));
                        return new SexpProc(env, argsSymbolList.ToArray(), cdr.Cdr.List);
                        break;
                    default:
                        maybeCallable = symbol.Evaluate(env, null);
                        break;
                }

                break;
            case SexpList list:
                maybeCallable = list.Evaluate(env, null);
                break;
        }

        // If things are ok, then evaluate args
        var evaluatedArgs = new SexpList() { List = Cdr.List.Select(arg => arg.Evaluate(env, null)).ToArray() };
        
        var result = maybeCallable switch
        {
            SexpCsharpProc proc => proc.Proc(env, evaluatedArgs),
            SexpProc proc => proc.Evaluate(env, evaluatedArgs),
        };
        return result;
    }

    public override SexpList ToList() => this;
}

public class SexpProc : Sexp
{
    protected virtual IEnumerable<SexpSymbol> Args { get; }
    protected virtual Environment Environment { get; } = new();
    protected virtual IEnumerable<Sexp> Body { get; }

    public SexpProc(Environment parent, IEnumerable<SexpSymbol> args, IEnumerable<Sexp> body)
    {
        Environment.Parent = parent;
        Args = args;
        Body = body;
    }
    public override string ToString() => "A proc";

    /// <summary>
    /// Lambda formals have 3 types of parameters that can go in the list
    /// required, optional and rest - here's the MIT document about em,
    /// Required
    ///   All of the required parameters are matched against the arguments first. If there are fewer arguments than required parameters, an error of type condition-type:wrong-number-of-arguments is signalled; this error is also signalled if there are more arguments than required parameters and there are no further parameters.
    /// Optional
    ///   Once the required parameters have all been matched, the optional parameters are matched against the remaining arguments. If there are fewer arguments than optional parameters, the unmatched parameters are bound to special objects called default objects. If there are more arguments than optional parameters, and there are no further parameters, an error of type condition-type:wrong-number-of-arguments is signalled. The predicate default-object?, which is true only of default objects, can be used to determine which optional parameters were supplied, and which were defaulted.
    /// Rest
    ///   Finally, if there is a rest parameter (there can only be one), any remaining arguments are made into a list, and the list is bound to the rest parameter. (If there are no remaining arguments, the rest parameter is bound to the empty list.) In Scheme, unlike some other Lisp implementations, the list to which a rest parameter is bound is always freshly allocated. It has infinite extent and may be modified without affecting the procedure's caller.
    ///
    /// Specially recognized keywords divide the formals parameters into these three classes. The keywords used here are `#!optional', `.', and `#!rest'. Note that only `.' is defined by standard Scheme -- the other keywords are MIT Scheme extensions. `#!rest' has the same meaning as `.' in formals.
    /// These are the examples given:
    /// (a b c)
    ///   a, b, and c are all required. The procedure must be passed exactly three arguments.
    /// (a b #!optional c)
    ///   a and b are required, c is optional. The procedure may be passed either two or three arguments.
    /// (#!optional a b c)
    ///   a, b, and c are all optional. The procedure may be passed any number of arguments between zero and three, inclusive.
    /// a
    ///   (#!rest a)
    /// These two examples are equivalent. a is a rest parameter. The procedure may be passed any number of arguments. Note: this is the only case in which `.' cannot be used in place of `#!rest'.
    /// (a b #!optional c d #!rest e)
    ///   a and b are required, c and d are optional, and e is rest. The procedure may be passed two or more arguments.  
    /// </summary>
    /// <param name="env"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    /// <exception cref="SexpException"></exception>
    public override Sexp Evaluate(Environment env, SexpList args)
    {
        // Currently only required args are supported, for the moment
        foreach (var (argName, argVal) in Args.Zip(args.List))
            Environment.Values[argName.Value] = argVal;

        Sexp result = SexpUndefined.Undefined;
        foreach (var exp in Body)
            result = exp.Evaluate(Environment, null);

        return result;
    }

    public override SexpProc ToProc() => this;
}

public class SexpCsharpProc : Sexp
{
    public SexpCsharpProc(Func<Environment, SexpList, Sexp> proc)
    {
        Proc = proc;
    }

    public virtual Func<Environment, SexpList, Sexp> Proc { get; set; }

    public override string ToString() => "A C# proc";

    public override Sexp Evaluate(Environment env, SexpList args)
    {
        return Proc(env, args);
    }
}

public class SexpUndefined : Sexp
{
    private SexpUndefined()
    {
    }

    public static readonly SexpUndefined Undefined = new SexpUndefined();

    public override string ToString() => "<undefined>";

    public override Sexp Evaluate(Environment env, SexpList args)
    {
        throw new SexpException("cannot evaluate undefined");
    }
}