﻿namespace Scheme;

public class Environment
{
    public Dictionary<string, Sexp> Values = new();
    public Environment? Parent { get; set; }
    public Sexp? Lookup(string name)
    {
        if (Values.TryGetValue(name, out var output))
            return output;
        return Parent?.Lookup(name);
    }

    public static Environment Default()
    {
        var env = new Environment();
        env.Values.Add("+", new SexpCsharpProc(SumFunc));
        env.Values.Add("*", new SexpCsharpProc(MulFunc));
        env.Values.Add("-", new SexpCsharpProc(SubFunc));
        env.Values.Add("eq?", new SexpCsharpProc(EqFunc));

        return env;
    }

    private static Sexp SumFunc(Environment env, SexpList args)
    {
        float result = 0;
        foreach (var sexp in args.List)
        {
            result += sexp switch
            {
                SexpNum num => num.Value,
                _ => throw new SexpException("only numbers can be added with +"),
            };
        }

        return new SexpNum() { Value = result };
    }
    
    private static Sexp MulFunc(Environment env, SexpList args)
    {
        float result = 1;
        foreach (var sexp in args.List)
        {
            result *= sexp switch
            {
                SexpNum sexpNum => sexpNum.Value,
                _ => throw new SexpException("only numbers can be added with +"),
            };
        }

        return new SexpNum() { Value = result };
    }

    private static Sexp SubFunc(Environment env, SexpList args)
    {
        float result = args.List[0] switch
        {
            SexpNum num => num.Value,
            _ => throw new SexpException("numbers expected in call to -"),
        };
        foreach (var sexp in args.List[1..])
        {
            result -= sexp switch
            {
                SexpNum num => num.Value,
                _ => throw new SexpException("numbers expected in call to -"),
            };
        }

        return new SexpNum() { Value = result };
    }

    private static Sexp EqFunc(Environment env, SexpList args)
    {
        float args1 = args.Car switch
        {
            SexpNum num => num.Value,
            _ => throw new SexpException("expected number in eq?"),
        };
        float args2 = args.List[1] switch
        {
            SexpNum num => num.Value,
            _ => throw new SexpException("expected number in eq?"),
        };
        return new SexpBool() { Value = args1 == args2 };
    }

    public Sexp Define(SexpList args)
    {
        // Define has 2 special forms:
        // 1. define: (define x value)
        // 2. procedure define: (define (proc-name arg1 arg2) expression expresion ...)
        switch (args.Car)
        {
            case SexpList list:
                // proc define
                // the name of the proc is the lists car
                var procName = list.Car switch
                {
                    SexpSymbol symbol => symbol.Value,
                    _ => throw new SexpException("proc define expects first element of formals to be a symbol"),
                };
                // the remainder are the args
                var procArgs = list.Cdr;
                var argsSymbolList = procArgs.List.Select(arg =>
                    arg is SexpSymbol sym ? sym : throw new SexpException("lambda expects list of symbol"));
                // the cdr of args is the procedures body
                Values[procName] = new SexpProc(this, argsSymbolList, args.Cdr.List);
                break;
            case SexpSymbol symbol:
                // regular define
                Values[symbol.Value] = args.List[1].Evaluate(this, args);
                break;
            default:
                throw new SexpException("define requires a symbol or list of symbol as first argument");
        }
        return SexpUndefined.Undefined;
    }
}
