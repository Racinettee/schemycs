using System;
using System.Text;
using System.IO;

namespace Scheme;

public class Parser
{
    private const string validIdentifierChars = "!?#^+/-*_";

    public Sexp ParseString(string expression)
    {
        return Parse(new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(expression))));
    }

    public List<Sexp> ParseFile(string path)
    {
        if (!File.Exists(path))
            throw new Exception($"{path}: file does not exist");
        
        using var file = File.OpenRead(path);
        var reader = new StreamReader(file);

        var result = new List<Sexp>();
        
        while (!reader.EndOfStream)
            result.Add(Parse(reader));

        return result;
    }
    /// <summary>
    /// Parse parses a single potentially compound expression
    /// </summary>
    /// <param name="reader"></param>
    /// <returns>The sexp for the expression</returns>
    public Sexp Parse(StreamReader reader)
    {
        while (char.IsWhiteSpace((char)reader.Peek()))
            reader.Read();
        var ch = (char)reader.Read();
        
        if (ch == '(')
        {
            var results = new List<Sexp>();
            // May need to combine this conditoin with a white space chomp
            // so that it can't be tricked by " .... )"
            while (reader.Peek() != ')')
                results.Add(Parse(reader));
            // Read past the ) so that the next expression can be parsed
            reader.Read();
            return new SexpList() { List = results.ToArray(), };
        }
        if (char.IsDigit(ch))
            return ParseNumber(reader, ch);

        if (char.IsLetter(ch) || validIdentifierChars.Contains(ch))
            return ParseSymbol(reader, ch);

        return SexpUndefined.Undefined;
    }

    SexpNum ParseNumber(StreamReader reader, char firstDigit)
    {
        StringBuilder resultStr = new StringBuilder();
        resultStr.Append(firstDigit);

        char ch = ' ';
        while (char.IsDigit(ch = (char)reader.Peek()) || ch == '.')
        {
            reader.Read();
            resultStr.Append(ch);
        }

        return new SexpNum() { Value = float.Parse(resultStr.ToString()) };
    }

    Sexp ParseSymbol(StreamReader reader, char firstLetter)
    {
        StringBuilder resultStr = new StringBuilder();
        resultStr.Append(firstLetter);

        char ch = ' ';
        while (char.IsLetter(ch = (char)reader.Peek()) || char.IsDigit(ch) || validIdentifierChars.Contains(ch))
        {
            reader.Read();
            resultStr.Append(ch);
        }

        return resultStr.ToString() switch
        {
            "#t" => new SexpBool() { Value = true, },
            "#f" => new SexpBool() { Value = false, },
            _ => new SexpSymbol() { Value = resultStr.ToString() },
        };
    }
}