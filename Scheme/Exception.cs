﻿namespace Scheme;

public class SexpException : Exception
{
    public string Message { get; }
    public SexpException(string reason)
    {
        Message = reason;
    }
}